#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlDatabase>
#include <QDir>

bool openDb(QSqlDatabase &db)
{
    db = QSqlDatabase::addDatabase("QSQLITE");
#if QT_VERSION < 0x050000
    QDir dir(QDesktopServices::storageLocation(QDesktopServices::DataLocation));
#else
    QDir dir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
#endif
    dir.mkpath(".");
    QString path(dir.filePath("bitbot.sqlite"));
    db.setDatabaseName(path);

    if (!db.open())
    {
        qCritical() << "Could not open database:" << db.lastError().text();
        return false;
    }
    return true;
}

bool MainWindow::getTicker(quint64 time, Ticker &ticker)
{
    QSqlQuery query;

    query.prepare("SELECT * FROM ticker WHERE time < ? ORDER BY time DESC LIMIT 1;");
    query.addBindValue(time);

    if (!query.exec()) {
        qCritical() << "Query error1: " << query.lastError().text();
        return false;
    }

    if (query.next())
    {
        ticker.date = QDateTime::fromMSecsSinceEpoch(query.value(0).toULongLong());
        ticker.price.price = query.value(1).toReal();
        ticker.price.buy = query.value(2).toReal();
        ticker.price.sell = query.value(3).toReal();
        ticker.high = query.value(4).toReal();
        ticker.low = query.value(5).toReal();
        ticker.volume = query.value(6).toReal();
        ticker.monthAvg = query.value(7).toReal();
        ticker.monthVolume = query.value(8).toReal();
        ticker.remote = BTC;
        ticker.local = SEK;

        return true;
    }
    qCritical() << "Query error22: " << query.lastError().text();

    return false;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QCoreApplication::setOrganizationName("Mhn");
    QCoreApplication::setApplicationName("BitBot");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    QSqlDatabase db;

    if (!openDb(db))
    {
        qCritical() << "could not open db";
        return;
    }


    QSqlQuery query;
    query.prepare("SELECT * FROM ticker ORDER BY time;");

    if (!query.exec()) {
        qCritical() << "Query error1: " << query.lastError().text();
    }

    QVector<double> buy, price, sell, time;

    Price max = {0, 0, 0}, min = {INT_MAX, INT_MAX, INT_MAX};
    qint64 from = QDateTime::currentDateTime().toMSecsSinceEpoch(), to = 0;

    while (query.next() && buy.size() < 250)
    {
        Ticker ticker;
        ticker.date = QDateTime::fromMSecsSinceEpoch(query.value(0).toULongLong());
        ticker.price.price = query.value(1).toReal();
        ticker.price.buy = query.value(2).toReal();
        ticker.price.sell = query.value(3).toReal();
        ticker.high = query.value(4).toReal();
        ticker.low = query.value(5).toReal();
        ticker.volume = query.value(6).toReal();
        ticker.monthAvg = query.value(7).toReal();
        ticker.monthVolume = query.value(8).toReal();
        ticker.remote = BTC;
        ticker.local = SEK;

//        qDebug() << ticker.date << to << from;

        buy.push_back(ticker.price.buy);
//        sell.push_back(ticker.price.sell);
//        price.push_back(ticker.price.price);
        time.push_back(ticker.date.toMSecsSinceEpoch());

        if (ticker.price.buy > max.buy)
            max.buy = ticker.price.buy;
        else if (ticker.price.buy < min.buy)
            min.buy = ticker.price.buy;

        if (ticker.price.sell > max.sell)
            max.sell = ticker.price.sell;
        else if (ticker.price.sell < min.sell)
            min.sell = ticker.price.sell;

        if (ticker.price.price > max.price)
            max.price = ticker.price.price;
        else if (ticker.price.price < min.price)
            min.price = ticker.price.price;

        if (ticker.date.toMSecsSinceEpoch() > to)
            to = ticker.date.toMSecsSinceEpoch();
        if (ticker.date.toMSecsSinceEpoch() < from)
            from = ticker.date.toMSecsSinceEpoch();
    }



    size_t interval = 30;
    size_t smoothness = 120;
    size_t size = (to - from) / interval - 1;
    qDebug() << "size" << size;
    QVector<double> buy2(size), time2(size);

    Ticker t;
    for (size_t i = 0; i < size; i++)
    {
        if (!getTicker(from + i*interval + 1, t))
        {
            qDebug() << "the end";
            break;
        }
        buy2[i] = t.price.buy;
//        sell2[i] = t.price.sell;
//        price2[i] = t.price.price;
        time2[i] = t.date.toMSecsSinceEpoch();
    }
    for (size_t i = size - 1; i >= smoothness; i--)
    {
        for (size_t j = 1; j < smoothness; j++)
        {
            buy2[i] += buy2[i-j];
        }
        buy2[i] /= smoothness;
    }


    // create graph and assign data to it:
    ui->widget->addGraph();
    ui->widget->graph(0)->setData(time, buy);
    ui->widget->graph(0)->setPen(QPen(Qt::blue));
    // create graph and assign data to it:
    ui->widget->addGraph();
    ui->widget->graph(1)->setData(time2, buy2);
    ui->widget->graph(1)->setPen(QPen(Qt::green));
    // create graph and assign data to it:
//    ui->widget->addGraph();
//    ui->widget->graph(2)->setData(time, price);
//    ui->widget->graph(2)->setPen(QPen(Qt::red));
    // give the axes some labels:
    ui->widget->xAxis->setLabel("x");
    ui->widget->yAxis->setLabel("y");
    // set axes ranges, so we see all data:
    ui->widget->xAxis->setRange(from, to);
    ui->widget->yAxis->setRange(qMin(qMin(min.price, min.buy), min.sell), qMax(qMax(max.price, max.buy), max.sell));
    ui->widget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);

    ui->widget->replot();
}

MainWindow::~MainWindow()
{
    delete ui;
}
