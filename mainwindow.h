#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDateTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT


    enum Currency { SEK, BTC, USD, PPC, EUR, UndefinedCurrency };

    struct Price {
        qreal price, buy, sell;
    };

    struct Ticker {
        Price price;
        QDateTime date;
        Currency local, remote;
        qreal high, low, volume, monthAvg, monthVolume;
    };

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool getTicker(quint64 time, Ticker &ticker);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
